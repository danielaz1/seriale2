package db;

import domain.Actor;

import java.sql.*;
import java.util.ArrayList;

public class TvSeriesActorManager {

	private static final String DELETE_BY_TVS_ID = "DELETE FROM TVSERIES_ACTOR WHERE TVS_ID =?";
	private static final String DELETE_BY_ACTOR_ID = "DELETE FROM TVSERIES_ACTOR WHERE ACTOR_ID=?";
	private static final String TABLE_NAME = "TVSERIES_ACTOR";
	private static final String CREATE_TABLE = "CREATE TABLE tvseries_actor (tvs_id bigint, actor_id bigint)";
	private static final String SELECT_ACTORS_ID = "SELECT ACTOR_ID FROM TVSERIES_ACTOR WHERE TVS_ID=?";
	private static final String INSERT = "INSERT INTO TVSERIES_ACTOR (TVS_ID, ACTOR_ID) VALUES (?,?)";

	private Connection connection;
	private PreparedStatement selectActorsIdStmt;
	private PreparedStatement insertStmt;
	private PreparedStatement deleteByTvsIdStmt;
	private PreparedStatement deleteByActorIdStmt;

	public TvSeriesActorManager() {

		connection = new HsqldbConnection().getConnection();
		try {
			Statement statement = connection.createStatement();
			if (! tableExists()) {
				statement.executeUpdate(CREATE_TABLE);
			}
			selectActorsIdStmt = connection.prepareStatement(SELECT_ACTORS_ID);
			insertStmt = connection.prepareStatement(INSERT);
			deleteByTvsIdStmt = connection.prepareStatement(DELETE_BY_TVS_ID);
			deleteByActorIdStmt = connection.prepareStatement(DELETE_BY_ACTOR_ID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private boolean tableExists() throws SQLException {
		ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		while (rs.next()) {
			if (TABLE_NAME.equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
				return true;
			}
		}
		return false;
	}

	public ArrayList<Actor> getActorsByTvsID(int tvSeriesId) throws SQLException {
		ActorManager actorManager = new ActorManager();
		ArrayList<Actor> actors = new ArrayList<>();

		selectActorsIdStmt.setInt(1, tvSeriesId);

		ResultSet rs = selectActorsIdStmt.executeQuery();

		while (rs.next()) {
			int actorId = rs.getInt("actor_id");
			Actor actor = actorManager.getActorById(actorId);
			actors.add(actor);
		}
		return actors;
	}

	public void addActors(ArrayList<Actor> actors, int tvSeriesId) throws SQLException {
		ActorManager actorManager = new ActorManager();
		for (Actor actor : actors) {
			int actorId = actorManager.addActor(actor);
			insertStmt.setInt(1, tvSeriesId);
			insertStmt.setInt(2, actorId);
			insertStmt.executeUpdate();
		}
	}

	public void deleteByTvsId(int tvSeriesId) throws SQLException {
		deleteByTvsIdStmt.setInt(1, tvSeriesId);
		deleteByTvsIdStmt.executeUpdate();
	}

	public void deleteByActorId(int actorId) throws SQLException {
		deleteByActorIdStmt.setInt(1, actorId);
		deleteByActorIdStmt.executeUpdate();
	}
}
