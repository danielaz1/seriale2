package db;

import java.sql.*;

public abstract class HSQLDBManager {

	protected Connection connection;
	protected Statement statement;
	protected PreparedStatement addValuesStmt;
	protected PreparedStatement getAllValuesStmt;
	protected PreparedStatement deleteAllValuesStmt;
	protected PreparedStatement deleteRecordStmt;
	protected PreparedStatement updateStmt;

	public HSQLDBManager() {
		try {
			connection = new HsqldbConnection().getConnection();
			statement = connection.createStatement();

			if (! tableExists()) {
				statement.executeUpdate(getCreateTableQuery());
			}

			prepareStatements();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void prepareStatements() throws SQLException {
		addValuesStmt = connection.prepareStatement(getInsertQuery(), Statement.RETURN_GENERATED_KEYS);
		getAllValuesStmt = connection.prepareStatement(getAllValuesQuery());
		deleteRecordStmt = connection.prepareStatement(getDeleteRecordQuery());
		deleteAllValuesStmt = connection.prepareStatement(getDeleteAllValuesQuery());
		updateStmt = connection.prepareStatement(getUpdateQuery());
	}

	private boolean tableExists() throws SQLException {
		ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		while (rs.next()) {
			if (getTableName().equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
				return true;
			}
		}
		return false;
	}

	public Connection getConnection() {
		return connection;
	}

	public void deleteAllRecords() {
		try {
			deleteAllValuesStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void deleteRecord(int id) {
		try {
			deleteRecordStmt.setInt(1, id);
			deleteRecordStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected String getDeleteAllValuesQuery() {
		return "DELETE FROM " + getTableName();
	}

	protected String getDeleteRecordQuery() {
		return "DELETE FROM " + getTableName() + " WHERE ID=?";
	}

	protected abstract String getCreateTableQuery();

	protected abstract String getTableName();

	protected abstract String getInsertQuery();

	protected abstract String getAllValuesQuery();

	protected abstract String getUpdateQuery();

}
