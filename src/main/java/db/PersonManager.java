package db;

import domain.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class PersonManager extends HSQLDBManager {

	public PersonManager() {
		super();
	}

	protected int addPerson(Person person) {
		int key = 0;
		try {
			addValuesStmt.setString(1, person.getName());
			addValuesStmt.setDate(2, java.sql.Date.valueOf(person.getDateOfBirth()));
			addValuesStmt.setString(3, person.getBiography());
			addValuesStmt.executeUpdate();

			ResultSet generatedKeys = addValuesStmt.getGeneratedKeys();
			if (generatedKeys.next()) {
				key = generatedKeys.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return key;
	}

	protected void updatePerson(Person person) {
		try {
			updateStmt.setString(1, person.getName());
			updateStmt.setDate(2, java.sql.Date.valueOf(person.getDateOfBirth()));
			updateStmt.setString(3, person.getBiography());
			updateStmt.setInt(4, person.getId());
			updateStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deletePerson(Person person) {
		deleteRecord(person.getId());
	}
}
