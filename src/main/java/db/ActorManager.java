package db;

import domain.Actor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ActorManager extends PersonManager {

	private static final String SELECT_BY_ID = "SELECT * FROM ACTOR WHERE ID=?";
	private PreparedStatement selectActorByIdStmt;

	public ActorManager() {
		super();
		try {
			selectActorByIdStmt = connection.prepareStatement(SELECT_BY_ID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Actor> getAllActors() {
		ArrayList<Actor> actors = new ArrayList<>();
		try {
			ResultSet rs = getAllValuesStmt.executeQuery();
			while (rs.next()) {
				Actor actor = getActor(rs);
				actors.add(actor);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return actors;
	}

	public int addActor(Actor actor) {
		return addPerson(actor);
	}

	public void updateActor(Actor actor) {
		updatePerson(actor);
	}

	public void deleteActor(Actor actor) {
		TvSeriesActorManager tvSeriesActorManager = new TvSeriesActorManager();
		try {
			tvSeriesActorManager.deleteByActorId(actor.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		deletePerson(actor);
	}

	@Override
	protected String getCreateTableQuery() {
		return "CREATE TABLE actor (id bigint GENERATED BY DEFAULT AS IDENTITY, name varchar(20), date_of_birth date, biography varchar(200))";
	}

	@Override
	protected String getTableName() {
		return "actor";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO actor (name, date_of_birth, biography) VALUES (?,?,?)";
	}

	@Override
	protected String getAllValuesQuery() {
		return "SELECT id, name, date_of_birth, biography FROM actor";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE actor SET name=?, date_of_birth=?, biography=? WHERE id=?";
	}

	public Actor getActorById(int actorId) throws SQLException {
		selectActorByIdStmt.setInt(1, actorId);
		Actor actor = new Actor();

		ResultSet rs = selectActorByIdStmt.executeQuery();
		if (rs.next()) {
			actor = getActor(rs);
		}
		return actor;
	}

	private Actor getActor(ResultSet rs) throws SQLException {
		Actor actor = new Actor();
		int actorId = rs.getInt("id");
		actor.setId(actorId);
		actor.setName(rs.getString("name"));
		actor.setDateOfBirth(rs.getDate("date_of_birth").toLocalDate());
		actor.setBiography(rs.getString("biography"));
		return actor;
	}
}
