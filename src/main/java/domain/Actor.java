package domain;

import java.time.LocalDate;
import java.util.ArrayList;

public class Actor extends Person {

	private ArrayList<TvSeries> tvSeries;

	public Actor(String name, LocalDate dateOfBirth, String biography) {
		super(name, dateOfBirth, biography);
	}

	public Actor() {

	}

}
