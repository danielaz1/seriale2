package domain;

import java.time.LocalDate;

public class Director extends Person {

	public Director() {
		super();
	}

	public Director(String name, LocalDate dateOfBirth, String biography) {
		super(name, dateOfBirth, biography);
	}

}
