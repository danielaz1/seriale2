package domain;

import java.time.Duration;
import java.time.LocalDate;

public class Episode {

	private int id;
	private String name;
	private int episodeNumber;
	private Duration duration;
	private LocalDate releaseDate;

	public Episode(String name, int episodeNumber, Duration duration, LocalDate releaseDate) {
		this.name = name;
		this.episodeNumber = episodeNumber;
		this.duration = duration;
		this.releaseDate = releaseDate;
	}

	public Episode() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(int episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public LocalDate getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
