package test;

import db.SeasonManager;
import domain.Season;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class SeasonManagerTest {

	private SeasonManager seasonManager = new SeasonManager();

	private static final int TVS_ID = 0;
	private static final int YEAR_1 = 2010;
	private static final int NUMBER_1 = 1;
	private static final int YEAR_2 = 2011;
	private static final int NUMBER_2 = 2;

	@Test
	public void checkOperations() {
		seasonManager.deleteAllRecords();
		Season season = new Season(NUMBER_1, YEAR_1);
		assertEquals(1, seasonManager.addSeason(season, TVS_ID));

		ArrayList<Season> seasons = new ArrayList<>();
		seasons = seasonManager.getAllSeasons();
		Season seasonRetrieved = seasons.get(0);
		assertEquals(NUMBER_1, seasonRetrieved.getSeasonNumber());
		assertEquals(YEAR_1, seasonRetrieved.getYearOfRelease());

		seasonRetrieved.setSeasonNumber(NUMBER_2);
		seasonRetrieved.setYearOfRelease(YEAR_2);
		seasonManager.updateSeason(seasonRetrieved);
		seasons = seasonManager.getAllSeasons();
		Season seasonRetrieved2 = seasons.get(0);
		assertEquals(NUMBER_2, seasonRetrieved2.getSeasonNumber());
		assertEquals(YEAR_2, seasonRetrieved2.getYearOfRelease());

		seasonManager.deleteSeason(seasonRetrieved2);
		seasons = seasonManager.getAllSeasons();
		assertEquals(0, seasons.size());
	}

}
