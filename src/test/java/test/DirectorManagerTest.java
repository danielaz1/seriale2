package test;

import db.DirectorManager;
import domain.Director;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class DirectorManagerTest {

	private DirectorManager directorManager = new DirectorManager();

	private static final int TVS_ID = 0;
	private final static String NAME_1 = "Name1";
	private final static String NAME_2 = "Name2";
	private final static String BIOGRAPHY_1 = "Biography1";
	private final static LocalDate DATE_1 = LocalDate.of(1990, 1, 1);

	@Test
	public void checkOperations() {
		directorManager.deleteAllRecords();
		Director director = new Director(NAME_1, DATE_1, BIOGRAPHY_1);
		assertEquals(1, directorManager.addDirector(director, TVS_ID));

		ArrayList<Director> directors = directorManager.getAllDirectors();
		Director directorRetrieved = directors.get(0);
		assertEquals(NAME_1, directorRetrieved.getName());
		assertEquals(BIOGRAPHY_1, directorRetrieved.getBiography());
		assertEquals(DATE_1, directorRetrieved.getDateOfBirth());

		directorRetrieved.setName(NAME_2);
		directorManager.updateDirector(directorRetrieved);
		directors = directorManager.getAllDirectors();
		Director directorRetrieved2 = directors.get(0);
		assertEquals(NAME_2, directorRetrieved2.getName());
		assertEquals(directorRetrieved.getId(), directorRetrieved2.getId());

		directorManager.deleteDirector(directorRetrieved2);
		directors = directorManager.getAllDirectors();
		assertEquals(0, directors.size());
	}

}
