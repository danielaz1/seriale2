package test;

import db.TvSeriesManager;
import domain.*;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TvSeriesManagerTest {

	private static final LocalDate DATE_1 = LocalDate.of(1990, 1, 1);
	private TvSeriesManager tvSeriesManager = new TvSeriesManager();

	private final static String NAME_1 = "Serial1";
	private final static String NAME_2 = "Serial2";

	@Test
	public void checkConnection() {
		assertNotNull(tvSeriesManager.getConnection());
	}

	@Test
	public void checkOperations() {
		tvSeriesManager.deleteAllRecords();
		TvSeries tvSeries = new TvSeries(NAME_1);
		tvSeriesManager.addTvSeries(tvSeries);

		ArrayList<TvSeries> tvs = new ArrayList<>();
		tvs = tvSeriesManager.getAllTvSeries();
		TvSeries tvsRetrieved = tvs.get(0);
		assertEquals(NAME_1, tvsRetrieved.getName());

		tvsRetrieved.setName(NAME_2);
		tvSeriesManager.updateTvSeries(tvsRetrieved);
		tvs = tvSeriesManager.getAllTvSeries();
		TvSeries tvsRetrieved2 = tvs.get(0);
		assertEquals(NAME_2, tvsRetrieved2.getName());

		tvSeriesManager.deleteTvSeries(tvsRetrieved2);
		tvs = tvSeriesManager.getAllTvSeries();
		assertEquals(0, tvs.size());
	}

	@Test
	public void checkTvs() {
		tvSeriesManager.deleteAllRecords();
		Episode episode = new Episode(NAME_1, 1, Duration.ofSeconds(9999), LocalDate.of(2000, 1, 1));
		ArrayList<Episode> episodes = new ArrayList<>();
		episodes.add(episode);

		Season season = new Season(1, 2010);
		season.setEpisodes(episodes);
		ArrayList<Season> seasons = new ArrayList<>();
		seasons.add(season);

		TvSeries tvSeries = new TvSeries(NAME_1);

		Actor actor = new Actor();
		actor.setName(NAME_1);
		actor.setDateOfBirth(DATE_1);
		ArrayList<Actor> actors = new ArrayList<>();
		actors.add(actor);

		Director director = new Director();
		director.setName(NAME_1);
		director.setDateOfBirth(DATE_1);

		tvSeries.setSeasons(seasons);
		tvSeries.setActors(actors);
		tvSeries.setDirector(director);
		tvSeriesManager.addTvSeries(tvSeries);

		ArrayList<TvSeries> tvs = new ArrayList<>();
		tvs = tvSeriesManager.getAllTvSeries();
		TvSeries tvsRetrieved = tvs.get(0);
		seasons = tvsRetrieved.getSeasons();
		assertNotNull(seasons);
		assertEquals(1, seasons.size());

		actors = tvsRetrieved.getActors();
		assertNotNull(actors);
		assertEquals(1, actors.size());

		Season seasonRetrieved = seasons.get(0);
		episodes = seasonRetrieved.getEpisodes();
		assertNotNull(episodes);
		assertEquals(1, episodes.size());

		tvSeriesManager.deleteTvSeries(tvsRetrieved);
		tvs = tvSeriesManager.getAllTvSeries();
		assertEquals(0, tvs.size());
	}

}
