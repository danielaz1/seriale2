package test;

import db.EpisodeManager;
import domain.Episode;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class EpisodeManagerTest {

	private EpisodeManager episodeManager = new EpisodeManager();

	private final static String NAME_1 = "Name1";
	private final static int NUMBER_1 = 1;
	private final static int NUMBER_2 = 2;
	private final static int SEASON_ID = 0;
	private final static LocalDate DATE_1 = LocalDate.of(1990, 1, 1);
	private final static Duration DURATION_1 = Duration.ofSeconds(9999);

	@Test
	public void checkOperations() {
		episodeManager.deleteAllRecords();
		Episode episode = new Episode(NAME_1, NUMBER_1, DURATION_1, DATE_1);
		assertEquals(1, episodeManager.addEpisode(episode, SEASON_ID));

		ArrayList<Episode> episodes = new ArrayList<>();
		episodes = episodeManager.getAllEpisodes();
		Episode episodeRetrieved = episodes.get(0);
		assertEquals(NUMBER_1, episodeRetrieved.getEpisodeNumber());
		assertEquals(DATE_1, episodeRetrieved.getReleaseDate());
		assertEquals(DURATION_1, episodeRetrieved.getDuration());

		episodeRetrieved.setEpisodeNumber(NUMBER_2);
		episodeManager.updateEpisode(episodeRetrieved);
		episodes = episodeManager.getAllEpisodes();
		Episode episodeRetrieved2 = episodes.get(0);
		assertEquals(NUMBER_2, episodeRetrieved2.getEpisodeNumber());
		assertEquals(episodeRetrieved.getId(), episodeRetrieved2.getId());

		episodeManager.deleteEpisode(episodeRetrieved2);
		episodes = episodeManager.getAllEpisodes();
		assertEquals(0, episodes.size());
	}

}
