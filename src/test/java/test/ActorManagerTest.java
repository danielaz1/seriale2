package test;

import db.ActorManager;
import db.TvSeriesActorManager;
import domain.Actor;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ActorManagerTest {

	private ActorManager actorManager = new ActorManager();

	private final static String NAME_1 = "Name1";
	private final static String NAME_2 = "Name2";
	private final static String BIOGRAPHY_1 = "Biography1";
	private final static LocalDate DATE_1 = LocalDate.of(1990, 1, 1);

	@Test
	public void checkOperations() {
		actorManager.deleteAllRecords();
		Actor actor = new Actor(NAME_1, DATE_1, BIOGRAPHY_1);
		actorManager.addActor(actor);
		ArrayList<Actor> actors = actorManager.getAllActors();
		Actor actorRetrieved = actors.get(0);
		assertEquals(NAME_1, actorRetrieved.getName());
		assertEquals(BIOGRAPHY_1, actorRetrieved.getBiography());
		assertEquals(DATE_1, actorRetrieved.getDateOfBirth());

		actorRetrieved.setName(NAME_2);
		actorManager.updateActor(actorRetrieved);
		actors = actorManager.getAllActors();
		Actor actorRetrieved2 = actors.get(0);
		assertEquals(NAME_2, actorRetrieved2.getName());
		assertEquals(actorRetrieved.getId(), actorRetrieved2.getId());

		actorManager.deleteActor(actorRetrieved2);
		actors = actorManager.getAllActors();
		assertEquals(0, actors.size());

		TvSeriesActorManager tvSeriesActorManager = new TvSeriesActorManager();
	}

}
